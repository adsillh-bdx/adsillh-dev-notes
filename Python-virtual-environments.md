---
title: Python virtual environments
---
# Installing packages

Python packages can be installed:

* In `/usr/lib/python3` if they were installed through the distribution's
  package manager (apt, dnf...)
* In `$HOME/.local/lib/python3.xx` if they were installed through pip (not
  running as the superuser)

One may not want to cause changes to their system by installing Python packages
in either of these two locations; instead, they can use self-contained virtual
environments.

# Using virtual environments

~~~console
$ python3 -m venv myenv             # Create the environment
$ source myenv/bin/activate         # Enter the environment
$ (myenv) pip list                  # Notice the prompt changed
$ deactivate                        # Leave the environment, back to "normal"
$ pip list                          # Will show a different list of packages
~~~

# More tools
* [Tox](Tox.md) is a standard tool to run tests inside virtual environments
