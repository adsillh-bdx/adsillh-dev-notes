---
title: Git
---
# Common commands

We assume the repository we are using:

* has a "main" branch named `master`
* the remote pointing the upstream repository is named `origin`

## Initial config

The global Git config may be found in `$XDG_CONFIG_HOME/git/config`.

~~~console
$ git config --global user.name "Firstname Lastname"
$ git config --global user.email "firstname.lastname@machine"
~~~

## Cloning a repository
~~~console
$ git clone $URL
$ cd $DIR
~~~

Add a remote pointing to your repository:

~~~console
$ git remote add my-remote $REMOTE-URL
~~~

## Making a change
To write a patch for a Free Software project, follow this procedure:

~~~console
$ git branch
* master
$ git switch -c fix-bug-123  # Create branch fix-bug-123 and switch to it
$ git branch
* fix-bug-123
  master
$ $EDITOR foo.py
$ git add foo.py
$ git commit
$ git rebase master          # Apply all changes on this branch on top of master
~~~

Finally, push your branch to your remote repository:

~~~console
# Syntax: git push <REMOTE> <LOCAL-BRANCH>:<REMOTE-BRANCH>
$ git push my-remote fix-bug-123:fix-bug-123
~~~

GitHub should automatically give you the option to create a pull request from
the Web interface. Some projects might use other tools, such as
[Gerrit](https://www.gerritcodereview.com/).

After getting comments on your PR from a maintainer, here is what you should
do:

~~~console
$ git switch fix-bug-123
$ $EDITOR ...
$ git add ...
$ git commit --amend         # Edit latest commit rather than creating a new one
~~~

But the master branch may have changed since you last worked on the project.
Pull the changes and rebase your branch on top of the master branch:

~~~console
$ git switch master
$ git pull origin master
$ git switch fix-bug-123
$ git rebase master
~~~

You may now push your changes again. Since this will overwrite your remote
branch, you may need to use the `--force` option:

~~~console
$ git push --force my-remote fix-bug-123:fix-bug-123
~~~

Do not force push shared branches.


## Other Useful commands

Look at the commits from the current branch, or from a given branch:

~~~console
$ git log
$ git log $BRANCH
~~~

Look at all the commits between two branches:

~~~console
$ git log master..fix-bug-123
~~~

Show the state of your working tree:

~~~console
$ git status
~~~

Grep code:

~~~console
$ git grep $PATTERN
~~~



# Useful links
* The [Git documentation](https://www.git-scm.com/doc)
* A [game](https://learngitbranching.js.org/) to learn Git branching
