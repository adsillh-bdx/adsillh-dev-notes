---
title: ADSILLH notes
---

# Topics
* [Git](Git.md)
* [GitHub CLI](GitHub-CLI.md)
* [Python virtual environments](Python-virtual-environments.md)
* [Tox](Tox.md)

# Technologies used to write these notes
* [VimWiki](https://github.com/vimwiki/vimwiki) to write these notes
* [Pandoc](https://pandoc.org/) to convert them to HTML
* The CSS was taken from [here](https://benjam.info/panam/)
* [This article](https://jamesbvaughan.com/markdown-pandoc-notes/) was helpful

# Building the HTML version of the notes
Typing `make` will build the HTML version of the notes in `./build`. The
`BUILDDIR` environment variable may also be used to specify another build
directory:

~~~console
$ make BUILDDIR=/tmp/build
~~~

# Editing the notes
The source may be found
[here](https://framagit.org/adsillh-bdx/adsillh-dev-notes). These notes use
Markdown files that can be edited with any text editor. One may also use
[VimWiki](https://github.com/vimwiki/vimwiki) if they so please:

~~~
let g:vimwiki_list = [
\ {
\ 'path': '/path/to/the/git/repository', 
\ 'path_html': '/path/to/the/build/directory',
\ 'custom_wiki2html': '/path/to/the/git/repository/vimwiki_to_html.sh',
\ 'css_name': '/dev/null',
\ 'syntax': 'markdown',
\ 'links_space_char': '-',
\ 'ext': '.md'
\ },
\]
~~~
