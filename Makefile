# Taken from https://jamesbvaughan.com/markdown-pandoc-notes/
BUILDDIR ?= build
MD_FILES=$(shell find . -name \*.md)
HTML_FILES=$(MD_FILES:.md=.html)
CSS_FILES=$(shell find css -name \*.css)
BUILD_HTML_FILES=$(HTML_FILES:%=${BUILDDIR}/%)
BUILD_CSS_FILES=$(CSS_FILES:%=${BUILDDIR}/%)

all: $(BUILD_HTML_FILES) $(BUILD_CSS_FILES)

${BUILDDIR}/css/%: css/%
	mkdir -p $$(dirname $@)
	cp $? $@

${BUILDDIR}/%.html: %.md template.html
	mkdir -p $$(dirname $@)
	pandoc \
		--from markdown \
		--to html5 \
		--toc \
		--css css/style.css \
		--highlight-style=pygments \
		--template=template.html \
		--lua-filter=links-to-html.lua -o $@ $<

clean:
	rm -rf ${BUILDDIR}

deploy:
	rsync --recursive --human-readable --delete --info=progress2 \
               ${BUILDDIR}/* aq-dionysos:public_html/adsillh-notes/
