---
title: Tox
---
# Overview

Tox is a de facto standard for managing Python virtual environments in order to
run tests. 

The configuration is written in `tox.ini` and the virtual environments are
stored in `.tox`.

# Common commands

## Listing available environments

You can list all environments that will be used by default when running `tox`:

~~~console
$ cd flask/
$ tox -l
py311
py310
py39
py38
py37
pypy38
pypy37
py310-min
py37-dev
style
typing
docs
~~~

You can list all defined environments, including those that will not be used by
default, by running `tox -a`.

## Running tests
You can run all tests by invoking `tox` with no arguments. This will create
virtual environments in the `.tox/` folder:

~~~console
$ tox
...
$ ls -1 .tox
dist
docs
log
py310
py310-min
py311
py37
py37-dev
py38
py39
style
typing
~~~

More often than not, you'll want to run only a subset of the tests:

~~~console
$ tox -epy310,py311
...
~~~
