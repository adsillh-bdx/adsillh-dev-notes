---
title: GitHub CLI
---

# Common commands

Cloning and forking a repository:

~~~console
$ gh repo fork --clone 
~~~

Listing issues

~~~console
$ gh issue list
~~~

Listing PRs

~~~console
$ gh pr list
~~~


# Useful links

* [Online documentation](https://cli.github.com/manual/gh_pr)
