#!/bin/bash

# FORCE=$1
# SYNTAX=$2
# EXTENSION=$3
OUTPUTDIR=$4
INPUT=$5
# CSSFILE=$6

OUTPUTFILE=${INPUT%.*}.html
OUTPUTFILE=$(basename "$OUTPUTFILE")
#OUTPUTFILE=$OUTPUTDIR/$(basename "$OUTPUTFILE")
TARGET=$OUTPUTDIR/$OUTPUTFILE


SRCDIR=$(dirname "$INPUT")
BUILDDIR="$OUTPUTDIR" make -C "$SRCDIR" "$TARGET"
