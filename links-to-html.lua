-- https://www.reddit.com/r/vim/comments/8jpew3/comment/e1o70gl
function Link(el)
  el.target = string.gsub(el.target, "%.md", ".html")
  return el
end
